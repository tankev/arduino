# Arduino

A sandbox for me to learn the basics of embedded programming with the Arduino UNO.

## Serial Printing

In order to debug the programs that you write for your Arduino, it's crucial that you know how to send information from your Arduino to your computer. This can be accomplished via serial printing. The specific communication protocol being used is RS232.

## Resources

- Arduino Documentation: [link](https://www.arduino.cc/reference/en/)
- Arduino Libraries: [link](https://www.arduino.cc/en/reference/libraries)
